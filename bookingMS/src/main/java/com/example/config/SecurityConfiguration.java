package com.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
@EnableAutoConfiguration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomAuthenticationProvider customAuthenticationProvider;

    private final DataSource dataSource;

    public SecurityConfiguration(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    // Configuration to point Spring Security to the database
    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(customAuthenticationProvider);

    }

    // Access configuration for the endpoints
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // To forbid access to /cars/**
        // TODO
        http.authorizeRequests()
                .antMatchers("/api/user/register").permitAll()
                .antMatchers("/api/**").authenticated()
                .anyRequest().permitAll()
                .and().httpBasic()
                .and().csrf().and().cors().disable();

    }

}
