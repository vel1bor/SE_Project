package com.example.domain;


import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String licencePlate;

    @Column(precision = 5, scale = 2)
    private BigDecimal pricePerDay;

    @Column(precision = 10, scale = 6)
    private BigDecimal longitude;

    @Column(precision = 10, scale = 6)
    private BigDecimal latitude;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "car")
    List<com.example.domain.Booking> bookings;

    public List<com.example.domain.Booking> getBookings() {
        if (bookings == null) {
            bookings = new ArrayList<>();
        }
        return bookings;
    }
}
