package com.example.config;

import com.example.resource.dtos.RabbitUserDto;
import com.example.utils.RabbitMQSenderCars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {


    @Autowired
    private RabbitMQSenderCars rabbitMQSenderCars;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        Object credentials = authentication.getCredentials();

        if (!(credentials instanceof String)) {
            return null;
        }
        String password = credentials.toString();
        // Call Authentification and check if user and password in db retunr user
        RabbitUserDto user = rabbitMQSenderCars.findUserByUsername(name, password);
//        boolean result = true;
        if(user != null){
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            return new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), grantedAuthorities);

        }else{
            return  null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
