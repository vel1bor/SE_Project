import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BookingListComponent} from './containers/booking-list/booking-list.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: BookingListComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookingRoutingModule {
}
