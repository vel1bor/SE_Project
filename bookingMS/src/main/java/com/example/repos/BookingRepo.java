package com.example.repos;

import com.example.domain.Booking;
import com.example.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookingRepo extends MongoRepository<Booking, String> {
    @Override
    List<Booking> findAll();

    List<Booking> findAllByUser(User user);

}
