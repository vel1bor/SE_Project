import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Car} from '../../../../shared/models/car.model';

@Component({
  selector: 'app-inner-card-list',
  templateUrl: './inner-car-list.component.html',
  styleUrls: ['./inner-car-list.component.css']
})
export class InnerCarListComponent implements OnInit {

  @Input() title: string;
  @Input() actionName: string;
  @Input() selectedCar: Car;
  @Input() cars: Car[];
  @Output() action = new EventEmitter<number>();
  @Output() carClick = new EventEmitter<Car>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onActionClicked(carId: number) {
    this.action.emit(carId);
  }

  onCarClicked(car: Car) {
    this.carClick.emit(car);
  }
}
