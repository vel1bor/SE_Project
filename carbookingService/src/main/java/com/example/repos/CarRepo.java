package com.example.repos;

import com.example.domain.Car;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CarRepo extends CrudRepository<Car, Long> {
    @Override
    List<Car> findAll();
}
