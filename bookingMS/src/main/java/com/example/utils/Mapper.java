package com.example.utils;

import com.example.domain.Booking;
import com.example.domain.Car;
import com.example.domain.User;
import com.example.resource.dtos.BookingDto;
import com.example.resource.dtos.CarDto;
import com.example.resource.dtos.RabbitUserDto;
import com.example.services.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class Mapper {

    private BookingService bookingService;

    @Autowired
    private RabbitMQSenderBooking sender;

    @Autowired
    public Mapper(final BookingService bookingService) {
        this.bookingService = bookingService;
    }

    public CarDto convertToDto(Car car) {
        CarDto carDto = new CarDto();
        carDto.setId(car.getId());
        carDto.setName(car.getName());
        carDto.setPrice(car.getPricePerDay());
        carDto.setLatitude(car.getLatitude());
        carDto.setLongitude(car.getLongitude());
        carDto.setLicencePlate(car.getLicencePlate());
        return carDto;
    }

    public BookingDto convertToDto(Booking booking) {
        BookingDto dto = new BookingDto();
        dto.setId(booking.getId());
        dto.setCarId(booking.getCar().getId());
        dto.setStartDate(booking.getStartDate());
        dto.setEndDate(booking.getEndDate());
        dto.setPrice(booking.getPrice());
        CarDto carDto = convertToDto(booking.getCar());
        dto.setCar(carDto);
        return dto;
    }

    public List<BookingDto> convertToDto(List<Booking> bookingList) {
        return bookingList
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private Booking convertToDomain(BookingDto bookingDto, Car car, User user) {
        Booking booking = new Booking();
        booking.setId(bookingDto.getId());
        booking.setCar(car);
        booking.setUser(user);
        booking.setStartDate(bookingDto.getStartDate());
        booking.setEndDate(bookingDto.getEndDate());

        return booking;
    }

    public Booking convertToDomain(BookingDto bookingDto, String username) {
        RabbitUserDto userDto = sender.findUserByUsername(username);
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setCurrency(userDto.getCurrency());
//        Optional<Car> car = carRepo.findById(bookingDto.getCarId());
        Car car = sender.findCarById(bookingDto.getCarId());
        return this.convertToDomain(bookingDto, car, user);
    }
}
