package com.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableAutoConfiguration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final DataSource dataSource;

    public SecurityConfiguration(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    // Configuration to point Spring Security to the database
    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .authoritiesByUsernameQuery("select username,password, enabled from users where username=?")
                .passwordEncoder(NoOpPasswordEncoder.getInstance());
    }

    // Access configuration for the endpoints
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // To forbid access to /cars/**
        http.authorizeRequests()
                .antMatchers("/api/currency").permitAll()
                .antMatchers("/api/user/register").permitAll()
                .antMatchers("/api/**").authenticated()
                .anyRequest().permitAll()
                .and().httpBasic()
                .and().csrf().disable();

    }

}
