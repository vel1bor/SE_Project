import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CardComponent} from './components/card/card.component';
import {NavPillsComponent} from './components/nav-pills/nav-pills.component';
import {RouterModule} from '@angular/router';

const COMPONENTS = [
  CardComponent,
  NavPillsComponent,
];
const IMPORTS = [
  CommonModule,
  RouterModule,
];

@NgModule({
  declarations: COMPONENTS,
  imports: IMPORTS,
  exports: COMPONENTS,
})
export class SharedModule {
}
