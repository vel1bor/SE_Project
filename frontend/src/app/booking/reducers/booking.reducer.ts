import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {Booking} from '../../shared/models/booking.model';
import {createReducer, on} from '@ngrx/store';
import {GetBookingsSuccess, StopBookingSuccess} from '../actions/booking.actions';

export interface BookingState extends EntityState<Booking> {
}

export const adapter: EntityAdapter<Booking> = createEntityAdapter<Booking>({
  selectId: e => e.id,
  sortComparer: (a, b) => {
    if (a.endDate === undefined) {
      return -1;
    }
    if (b.endDate === undefined) {
      return 1;
    }
    return 0;
  }
});

export const initialState: BookingState = adapter.getInitialState({});

const privateBookingReducer = createReducer(
  initialState,
  on(GetBookingsSuccess, (state, action) => adapter.addMany(action.payload.bookings, state)),
  on(StopBookingSuccess, (state, action) => adapter.updateOne({
    id: action.booking.id,
    changes: action.booking,
  }, state)),
);

export function bookingReducer(state, action) {
  return privateBookingReducer(state, action);
}

const {
  selectAll
} = adapter.getSelectors();

export const getBookings = selectAll;
