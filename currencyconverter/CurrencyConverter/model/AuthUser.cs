﻿using CurrencyConverter.context;
using CurrencyConverter.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;

namespace CurrencyConverter.model
{
    public class AuthUser : SoapHeader
    {
        public string username { get; set; }
        public string password { get; set; }

        public bool isValid()
        {
            UserService userService = new UserService();
            return userService.findUserByUsernameAndPassword(username, password);
        }
    }
}