import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap} from 'rxjs/operators';
import {
  BookCar,
  BookCarFailure,
  BookCarSuccess,
  GetAvailableCars,
  GetAvailableCarsFailure,
  GetAvailableCarsSuccess,
  ReturnCar,
  ReturnCarFailure,
  ReturnCarSuccess
} from '../actions/rental.actions';
import {RentalService} from '../services/rental.service';
import {of} from 'rxjs';
import {Car} from '../../shared/models/car.model';
import {Booking} from "../../shared/models/booking.model";

@Injectable({
  providedIn: 'root'
})
export class RentalEffects {

  onGetAvailableCars$ = createEffect(() => this.actions$
    .pipe(
      ofType(GetAvailableCars),
      switchMap(action => {
        return this.rentalService
          .getAvailableCars()
          .pipe(
            map((cars: Car[]) => {
              return GetAvailableCarsSuccess({payload: {cars}});
            }),
            catchError(reason => {
              return of(GetAvailableCarsFailure());
            })
          );
      }),
    )
  );

  onBookCar$ = createEffect(() => this.actions$
    .pipe(
      ofType(BookCar),
      switchMap(action =>
        this.rentalService
          .bookCar(action.carId)
          .pipe(
            map((booking: Booking) => {
              return BookCarSuccess({booking});
            }),
            catchError(reason => {
              return of(BookCarFailure());
            })
          )
      ),
    )
  );

  onReturnCar$ = createEffect(() => this.actions$
    .pipe(
      ofType(ReturnCar),
      switchMap(action =>
        this.rentalService
          .returnCar(action.carId)
          .pipe(
            map((car: Car) => {
              return ReturnCarSuccess({car});
            }),
            catchError(reason => {
              return of(ReturnCarFailure());
            })
          )
      ),
    )
  );

  constructor(
    private actions$: Actions,
    private rentalService: RentalService,
  ) {
  }
}
