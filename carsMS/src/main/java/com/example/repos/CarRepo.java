package com.example.repos;

import com.example.domain.Car;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CarRepo extends MongoRepository<Car, String> {
    @Override
    List<Car> findAll();
}
