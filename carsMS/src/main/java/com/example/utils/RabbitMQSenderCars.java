package com.example.utils;

import com.example.domain.Booking;
import com.example.domain.Car;
import com.example.domain.User;
import com.example.resource.dtos.RabbitUserDto;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

public class RabbitMQSenderCars {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private DirectExchange direct;

    public Booking getLatestBooking(Car car) {
        return (Booking) this.rabbitTemplate.convertSendAndReceive(direct.getName(), "bookings", car.getBookings());
    }

    public RabbitUserDto findUserByUsername(String username, String password) {
        return (RabbitUserDto) this.rabbitTemplate.convertSendAndReceive(direct.getName(), "users", new RabbitUserDto(username, password));
    }

    public RabbitUserDto findUserByUsername(String username) {
        return (RabbitUserDto) this.rabbitTemplate.convertSendAndReceive(direct.getName(), "users", new RabbitUserDto(username));
    }
}
