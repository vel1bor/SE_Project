package com.example.services;


import com.example.domain.Car;
import com.example.domain.User;
import com.example.repos.CarRepo;
import com.example.repos.UserRepo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class UserServiceTest {

    @Resource
    private UserRepo userRepo;


    @Test
    public void registerTest() {
        // given
        User user = new User();
        user.setUsername("testUser");
        user.setPassword("testPass");
        user.setCurrency("EUR");

        // when
        userRepo.save(user);

        // then
        List<User> result = new ArrayList<>();
        userRepo.findAll().forEach(u -> result.add(u));
        Assert.assertEquals("testUser", result.get(0).getUsername());

    }

    @Test
    public void getUserByUsernameTest() {
        // given
        User user = new User();
        user.setUsername("testUser");
        user.setPassword("testPass");
        user.setCurrency("EUR");
        userRepo.save(user);

        // when
        User result = userRepo.findByUsername("testUser")
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User does not exist"));

        // then
        Assert.assertEquals("testUser", result.getUsername());

    }




}