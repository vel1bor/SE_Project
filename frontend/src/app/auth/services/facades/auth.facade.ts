import {Injectable} from '@angular/core';
import {createSelector, select, Store} from '@ngrx/store';
import * as fromAuth from '../../reducers/auth.reducer';
import {AuthState} from '../../reducers/auth.reducer';
import {GetCurrencies, Register} from '../../actions/auth.actions';
import {User} from '../../models/user.model';
import * as fromRoot from '../../../store';

@Injectable({
  providedIn: 'root'
})
export class AuthFacade {

  private currencies$ = this.store.pipe(
    select(createSelector(
      fromRoot.getAuthState,
      fromAuth.getCurrencies,
    ))
  );

  constructor(
    private store: Store<AuthState>,
  ) {
  }

  public register(user: User) {
    this.store.dispatch(Register({user}));
  }

  getCurrencies() {
    this.store.dispatch(GetCurrencies());
    return this.currencies$;
  }
}
