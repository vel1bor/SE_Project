//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2020.04.20 um 05:45:58 PM CEST 
//


package com.example.soap;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.example.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.example.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConvertCurrencies }
     * 
     */
    public ConvertCurrencies createConvertCurrencies() {
        return new ConvertCurrencies();
    }

    /**
     * Create an instance of {@link ConvertCurrenciesResponse }
     * 
     */
    public ConvertCurrenciesResponse createConvertCurrenciesResponse() {
        return new ConvertCurrenciesResponse();
    }

    /**
     * Create an instance of {@link ConverterDTO }
     * 
     */
    public ConverterDTO createConverterDTO() {
        return new ConverterDTO();
    }

    /**
     * Create an instance of {@link GetCurrencyNames }
     * 
     */
    public GetCurrencyNames createGetCurrencyNames() {
        return new GetCurrencyNames();
    }

    /**
     * Create an instance of {@link GetCurrencyNamesResponse }
     * 
     */
    public GetCurrencyNamesResponse createGetCurrencyNamesResponse() {
        return new GetCurrencyNamesResponse();
    }

    /**
     * Create an instance of {@link CurrencyDTO }
     * 
     */
    public CurrencyDTO createCurrencyDTO() {
        return new CurrencyDTO();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

}
