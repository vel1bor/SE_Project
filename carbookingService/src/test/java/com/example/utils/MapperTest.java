package com.example.utils;

import com.example.domain.Booking;
import com.example.domain.Car;
import com.example.domain.User;
import com.example.resource.BaseResource;
import com.example.resource.dtos.BookingDto;
import com.example.resource.dtos.CarDto;
import com.example.resource.dtos.UserDto;
import com.example.services.BookingService;
import com.example.services.CarService;
import com.example.services.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class MapperTest {


    private BookingService bookingService;
    private CarService carService;
    private UserService userService;

    private Mapper mapper = new Mapper(bookingService, carService, userService);

    @Test
    public void carConvertToDtoTest() {
        // given
        Car car = new Car();
        car.setName("TestVW");

        // when
        CarDto result = mapper.convertToDto(car, false);

        // then
        Assert.assertEquals(car.getName(), result.getName());
    }

    @Test
    public void carConvertToDomainTest() {
        // given
        CarDto carDto = new CarDto();
        carDto.setName("TestVW");

        // when
        Car result = mapper.convertToDomain(carDto);

        // then
        Assert.assertEquals(carDto.getName(), result.getName());
//        Assert.assertEquals(carDto.getLicencePlate(), result.getLicencePlate());
    }

    @Test
    public void bookingConvertToDtoTest() {
        // given
        Booking booking = new Booking();
        booking.setId(new Long(1));
        Car car = new Car();
        car.setName("TestVW");
        booking.setCar(car);

        // when
        BookingDto result = mapper.convertToDto(booking);

        // then
        Assert.assertEquals(booking.getId(), result.getId());
        Assert.assertEquals(booking.getCar().getName(), result.getCar().getName());
    }

    @Test
    public void bookingsListConvertToDtoTest() {
        // given
        List<Booking> bookings = new ArrayList<>();
        Booking booking1 = new Booking();
        booking1.setId(new Long(1));
        Car car1 = new Car();
        car1.setName("TestVW");
        booking1.setCar(car1);
        Booking booking2 = new Booking();
        booking2.setId(new Long(2));
        Car car2 = new Car();
        car2.setName("TestFord");
        booking2.setCar(car2);
        bookings.add(booking1);
        bookings.add(booking2);

        // when
        List<BookingDto> result = mapper.convertToDto(bookings);

        // then
        Assert.assertEquals(booking1.getId(), result.get(0).getId());
        Assert.assertEquals(booking2.getId(), result.get(1).getId());
    }

}
