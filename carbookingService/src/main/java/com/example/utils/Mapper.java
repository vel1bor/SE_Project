package com.example.utils;

import com.example.domain.Booking;
import com.example.domain.Car;
import com.example.domain.User;
import com.example.resource.dtos.BookingDto;
import com.example.resource.dtos.CarDto;
import com.example.resource.dtos.UserDto;
import com.example.services.BookingService;
import com.example.services.CarService;
import com.example.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class Mapper {

    private BookingService bookingService;
    private CarService carService;
    private UserService userService;

    @Autowired
    public Mapper(final BookingService bookingService,
                  final CarService carService,
                  final UserService userService
    ) {
        this.bookingService = bookingService;
        this.carService = carService;
        this.userService = userService;
    }


    public CarDto convertToDto(Car car, boolean isBooked) {
        CarDto carDto = convertToDto(car);
        carDto.setBooked(isBooked);
        return carDto;
    }

    public CarDto convertToDto(Car car) {
        CarDto carDto = new CarDto();
        carDto.setId(car.getId());
        carDto.setName(car.getName());
        carDto.setPrice(car.getPricePerDay());
        carDto.setLatitude(car.getLatitude());
        carDto.setLongitude(car.getLongitude());
        carDto.setLicencePlate(car.getLicencePlate());
        return carDto;
    }

    public Car convertToDomain(CarDto carDto) {
        Car car = new Car();
        car.setId(carDto.getId());
        car.setName(carDto.getName());
        car.setPricePerDay(carDto.getPrice());
        car.setLatitude(carDto.getLatitude());
        car.setLongitude(carDto.getLongitude());
        return car;
    }

    public BookingDto convertToDto(Booking booking) {
        BookingDto dto = new BookingDto();
        dto.setId(booking.getId());
        dto.setCarId(booking.getCar().getId());
        dto.setStartDate(booking.getStartDate());
        dto.setEndDate(booking.getEndDate());
        dto.setPrice(booking.getPrice());
        CarDto carDto = convertToDto(booking.getCar());
        dto.setCar(carDto);
        return dto;
    }

    public List<BookingDto> convertToDto(List<Booking> bookingList) {
        return bookingList
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private Booking convertToDomain(BookingDto bookingDto, Car car, User user) {
        Booking booking = new Booking();
        booking.setId(bookingDto.getId());
        booking.setCar(car);
        booking.setUser(user);
        booking.setStartDate(bookingDto.getStartDate());
        booking.setEndDate(bookingDto.getEndDate());

        return booking;
    }

    public User convertToDomain(UserDto userDto) {
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setCurrency(userDto.getCurrency());
        user.setEnabled(userDto.isEnabled());
        return user;
    }

    public UserDto convertToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setCurrency(user.getCurrency());
        userDto.setEnabled(user.isEnabled());
        return userDto;
    }

    public Booking convertToDomain(BookingDto bookingDto, String username) {
        User user = userService.getUserByUsername(username);
        Car car = carService.getCarById(bookingDto.getCarId());
        return this.convertToDomain(bookingDto, car, user);
    }
}
