﻿using CurrencyConverter.DTO;
using CurrencyConverter.model;
using CurrencyConverter.service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace CurrencyConverter.webservice
{
    /// <summary>
    /// Zusammenfassungsbeschreibung für CurrencyConverterWebservice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Wenn der Aufruf dieses Webdiensts aus einem Skript zulässig sein soll, heben Sie mithilfe von ASP.NET AJAX die Kommentarmarkierung für die folgende Zeile auf. 
    // [System.Web.Script.Services.ScriptService]
    public class CurrencyConverterWebservice : System.Web.Services.WebService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        CurrencyConverterService currencyConverterService = new CurrencyConverterService();
        public AuthUser authUser;

        /// <summary>
        /// In this method the convertion from one currency into another one is performed.
        /// Then the result is mutliplied by the amount. 
        /// If the amount is less then 0, than we return 0.0.
        /// </summary>
        /// <param name="fromCurrency"></param>
        /// <param name="toCurrency"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [WebMethod]
        ///[SoapHeader("authUser")]
        public ConverterDTO convertCurrencies(string fromCurrency, string toCurrency, double amount)
        {
            ConverterDTO converterDTO = new ConverterDTO();
            try
            {
                converterDTO.result = currencyConverterService.convertCurrencies(fromCurrency, toCurrency, amount);
            }
            catch (WebException e)
            {
                log.Error("convertCurrencies(string fromCurrency, string toCurrency, double amount): " + e.Status + ". " + e.Message);
            }
            return converterDTO;
        }

        /// <summary>
        /// This method returns a list of all available currencies.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        ///[SoapHeader("authUser")]
        public CurrencyDTO getCurrencyNames()
        {
            CurrencyDTO currencyDTO = new CurrencyDTO();
            try
            {
                currencyDTO = currencyConverterService.getCurrencyNames();
            } catch (WebException e)
            {
                log.Error("getCurrencyNames(): " + e.Status + ". " + e.Message);
            }
            return currencyDTO;
        }
    }
}
