package com.example.resource;

import com.example.domain.Booking;
import com.example.resource.dtos.BookingDto;
import com.example.services.BookingService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.example.resource.BaseResource.BOOKING;

@Api()
@RestController()
@RequestMapping(BOOKING)
public class BookingResource extends BaseResource {

    private final BookingService bookingService;

    @Autowired
    public BookingResource(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PostMapping(value = "opening")
    public ResponseEntity<BookingDto> createBooking(@RequestBody BookingDto bookingDto) {
        if (bookingDto.getId() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID already set");
        }

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Booking booking = mapper.convertToDomain(bookingDto, username);
        BookingDto createdBookingDto = mapper.convertToDto(bookingService.createBooking(booking));
        return ResponseEntity.ok(createdBookingDto);
    }

    @PutMapping(value = "closing")
    public ResponseEntity<BookingDto> updateBooking(@RequestBody BookingDto bookingDto) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Booking booking = bookingService.updateBooking(bookingDto, username);
        BookingDto dto = mapper.convertToDto(booking);
        return ResponseEntity.ok(dto);
    }

    @GetMapping
    public ResponseEntity<List<BookingDto>> getBookings() {
        List<Booking> bookingsForUser = bookingService.getBookingsForUser(
                SecurityContextHolder.getContext().getAuthentication().getName()
        );
        return ResponseEntity.ok(mapper.convertToDto(bookingsForUser));
    }
}
