import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingListDumbComponent } from './booking-list-dumb.component';

describe('BookingListDumbComponent', () => {
  let component: BookingListDumbComponent;
  let fixture: ComponentFixture<BookingListDumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingListDumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingListDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
