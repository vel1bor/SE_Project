# CarsService

### Description
A simple and RESTfull service application for car renting. Users can register and login to get access to the main functions. 
These functions include fetching all available cars from the database, booking cars and returning them. 
The application is written in Java and it is based on the Spring Framework. By calling endpoints of the REST interface basic CRUD operations can be 
performed against the Postgres database. 

### Setup
 - Java 11
 - Maven
 - Postgres 10

### Manual
 1. Start Postgres database
 2. Configure database access points in /src/main/java/resources/application.properties
    (Optional) Configure test database access points in /src/test/java/resources/application.properties
 3. In Postgres create a database with a name according to the database access points. 
    (Optional) Create a test database with a name according to the test database access points.
 4. Run class App
 5. To register open following endpoint http://localhost:8080/register and pass an User object as JSON
 6. Open any endpoint and access it with your credentials.

### Endpoints
