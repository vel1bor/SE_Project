package com.example.resource.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserDto {
    @NotNull
    private String username;

    @NotNull
    private String password;

    private String currency;

    @JsonIgnore
    private boolean enabled = true;
}
