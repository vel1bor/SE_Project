import {Component, OnDestroy, OnInit} from '@angular/core';
import {Car} from '../../../shared/models/car.model';
import {RentalFacade} from '../../services/facades/rental.facade';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css']
})
export class CarListComponent implements OnInit, OnDestroy {

  allCars$;
  bookedCars$;
  selectedCar$;

  constructor(
    private facade: RentalFacade,
  ) {
  }

  ngOnInit() {
    this.allCars$ = this.facade.getAvailableCars();
    this.bookedCars$ = this.facade.getBookedCars();
    this.selectedCar$ = this.facade.getSelectedCar();
  }

  ngOnDestroy() {
    this.facade.deselectCar();
  }

  onCarBooked(carId: number) {
    this.facade.bookCar(carId);
  }

  onCarReturned(carId: number) {
    this.facade.returnCar(carId);
  }

  onCarSelected(car: Car) {
    this.facade.selectCar(car);
  }
}
