package com.example.resource.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class UserDto implements Serializable {
    @NotNull
    private String username;

    @NotNull
    private String password;

    private String currency;

    @JsonIgnore
    private boolean enabled = true;
}
