package com.example.utils;

import com.example.domain.Booking;
import com.example.domain.Car;
import com.example.domain.User;
import com.example.resource.dtos.RabbitUserDto;
import com.example.services.CarService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RabbitListener(queues = "cars")
public class RabbitMQReceiverCars {

    @Autowired
    private CarService carService;


    @RabbitListener(queues = "cars")
    public Car receive2(String carId) throws InterruptedException {
        return receive(carId);
    }

    public Car receive(String carId) {
        return carService.getCarById(carId);
    }
}
