import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnerCarListComponent } from './inner-car-list.component';

describe('InnerCardListComponent', () => {
  let component: InnerCarListComponent;
  let fixture: ComponentFixture<InnerCarListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InnerCarListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnerCarListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
