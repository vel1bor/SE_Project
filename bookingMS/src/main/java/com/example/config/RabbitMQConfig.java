package com.example.config;

import com.example.utils.RabbitMQReceiverBooking;
import com.example.utils.RabbitMQSenderBooking;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Bean
    public DirectExchange direct() {
        return new DirectExchange("tut.direct");
    }

    @Bean
    public Queue bookings() {
        return new Queue("bookings");
    }

    @Bean
    public RabbitMQSenderBooking sender() {
        return new RabbitMQSenderBooking();
    }

    @Bean
    public RabbitMQReceiverBooking receiver() {
        return new RabbitMQReceiverBooking();
    }

    @Bean
    public Binding bindingUsers(DirectExchange direct,
                                Queue bookings) {
        return BindingBuilder.bind(bookings)
                .to(direct)
                .with("bookings");
    }
}
