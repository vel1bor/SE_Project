import {createAction, props} from '@ngrx/store';
import {Car} from '../../shared/models/car.model';
import {Booking} from '../../shared/models/booking.model';

export enum RentalActionTypes {
  GetAvailableCars = '[CarListComponent] GetAvailableCars',
  GetAvailableCarsSuccess = '[API] GetAvailableCarsSuccess',
  GetAvailableCarsFailure = '[API] GetAvailableCarsFailure',

  BookCar = '[CarListComponent] BookCar',
  BookCarSuccess = '[API] BookCarSuccess',
  BookCarFailure = '[API] BookCarFailure',

  ReturnCar = '[CarListComponent] ReturnCar',
  ReturnCarSuccess = '[API] ReturnCarSuccess',
  ReturnCarFailure = '[API] ReturnCarFailure',

  SelectCar = '[CarListComponent] SelectCar',
  DeselectCar = '[CarListComponent] DeselectCar',
}

export const GetAvailableCars = createAction(
  RentalActionTypes.GetAvailableCars,
);
export const GetAvailableCarsSuccess = createAction(
  RentalActionTypes.GetAvailableCarsSuccess,
  props<{ payload: { cars: Car[] } }>()
);
export const GetAvailableCarsFailure = createAction(
  RentalActionTypes.GetAvailableCarsFailure,
);

export const BookCar = createAction(
  RentalActionTypes.BookCar,
  props<{ carId: number }>(),
);
export const BookCarSuccess = createAction(
  RentalActionTypes.BookCarSuccess,
  props<{ booking: Booking }>(),
);
export const BookCarFailure = createAction(
  RentalActionTypes.BookCarFailure,
);

export const ReturnCar = createAction(
  RentalActionTypes.ReturnCar,
  props<{ carId: number }>(),
);
export const ReturnCarSuccess = createAction(
  RentalActionTypes.ReturnCarSuccess,
  props<{ car: Car }>(),
);
export const ReturnCarFailure = createAction(
  RentalActionTypes.ReturnCarFailure,
);

export const SelectCar = createAction(
  RentalActionTypes.SelectCar,
  props<{ car: Car }>(),
);
export const DeselectCar = createAction(
  RentalActionTypes.DeselectCar,
);
