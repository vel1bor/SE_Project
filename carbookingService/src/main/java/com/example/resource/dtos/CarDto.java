package com.example.resource.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class CarDto {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String licencePlate;

    @NotNull
    private BigDecimal price;

    private boolean booked;

    @NotNull
    private BigDecimal longitude;

    @NotNull
    private BigDecimal latitude;
}
