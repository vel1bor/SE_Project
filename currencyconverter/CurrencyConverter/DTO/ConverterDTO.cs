﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyConverter.DTO
{
    public class ConverterDTO
    {
        public double result { get; set; }
    }
}