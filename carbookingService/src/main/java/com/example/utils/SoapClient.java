package com.example.utils;

import com.example.soap.ConvertCurrenciesResponse;
import com.example.soap.GetCurrencyNamesResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class SoapClient extends WebServiceGatewaySupport {

    public ConvertCurrenciesResponse convertCurrency(String url, Object request) {
        ConvertCurrenciesResponse res = (ConvertCurrenciesResponse) getWebServiceTemplate().marshalSendAndReceive(url, request,
                new SoapActionCallback("http://tempuri.org/convertCurrencies"));
        return res;
    }

    public GetCurrencyNamesResponse getCurrencies(String url, Object request) {
        GetCurrencyNamesResponse res = (GetCurrencyNamesResponse) getWebServiceTemplate().marshalSendAndReceive(url, request,
                new SoapActionCallback("http://tempuri.org/getCurrencyNames"));
        return res;
    }

}
