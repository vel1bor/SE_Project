package com.example.resource.dtos;

import java.io.Serializable;

public class RabbitUserDto implements Serializable {
    private String username;
    private String password;
    private String currency;

    public RabbitUserDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public RabbitUserDto(String username) {
        this.username = username;
    }

    public RabbitUserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
