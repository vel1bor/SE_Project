﻿using CurrencyConverter.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Xml.Serialization;

namespace CurrencyConverter.service
{
    public class CurrencyConverterService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        HttpClient httpClient = new HttpClient();
        private static String CURRENCY_URL = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

        private Envelope envelope;

        public CurrencyConverterService() {  }

        /// <summary>
        /// Get the envelope object if it is not already created.
        /// </summary>
        /// <returns></returns>
        private Envelope getEnvelope()
        {
            log.Info("getEnvelope(): Started!");
            if (envelope == null)
            {
                log.Info("getEnvelope(): Envelope is empty. Fetching Currencies.");
                envelope = getCurrencies();
            }
            log.Info("getEnvelope(): Ended!");
            return envelope;
        }

        /// <summary>
        /// Performs the get Request for the currency data. 
        /// </summary>
        /// <returns></returns>
        public Envelope getCurrencies()
        {
            log.Info("CurrencyConverterWebservice.getCurrencies(): Started!");

            httpClient.BaseAddress = new Uri(CURRENCY_URL);
            // Add an Accept header for XML format.    
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
            // List all Data from the webpage 
            HttpResponseMessage response = httpClient.GetAsync("").Result;  // Blocking call!    
            if (response.IsSuccessStatusCode)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Envelope));
                TextReader reader = new StringReader(response.Content.ReadAsStringAsync().Result);
                Envelope currencies = (Envelope)serializer.Deserialize(reader);
                reader.Close();
                log.Info("CurrencyConverterWebservice.getCurrencies(): Ende!");
                return currencies;
            }
            else
            {
                log.Error("CurrencyConverterWebservice.getCurrencies(): Status: " + response.StatusCode);
                log.Error("CurrencyConverterWebservice.getCurrencies(): ErrorMessage: " + response.ReasonPhrase);
                throw new WebException("CurrencyConverterWebservice.getCurrencies(): " + response.StatusCode + " ErrorMesssage: " + response.ReasonPhrase);
            }
        }

        /// <summary>
        /// This method performs the cross currency convertion.
        /// The two parameters represent the values of the given currencies to the EUR.
        /// For Example: 5 USD -> JPY
        /// toCurrencyRate is: 1 EUR = 117.12 JPY
        /// fromCurrencyRate is: 1 EUR = 1.0888 USD
        /// Amount is in this example 5 USD.
        /// </summary>
        /// <param name="toCurrencyRate"></param>
        /// <param name="fromCurrencyRate"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public double convertCrossCurrencies(double toCurrencyRate, double fromCurrencyRate, double amount)
        {
            log.Info("convertCrossCurrencies(double toCurrencyRate, double fromCurrencyRate, double amount): Started!");
            // calculate how much one USD is in EUR
            double fromCurrencyEUR = 1 / fromCurrencyRate;
            // calculate how much one JPY is in EUR
            double toCurrencyEUR = 1 / toCurrencyRate;

            // calculate how much one USD is in JPY
            double rateBetweenCurrencies = fromCurrencyEUR / toCurrencyEUR;

            log.Info("convertCrossCurrencies(double toCurrencyRate, double fromCurrencyRate, double amount): Ended!");
            // calculate the final result 5 USD -> JPY
            return amount * rateBetweenCurrencies;
        }

        /// <summary>
        /// This method finds the rate of the given currency.
        /// Returns -1 if the currency was not found.
        /// </summary>
        /// <param name="toCurrency"></param>
        /// <param name="envelope"></param>
        /// <returns></returns>
        public double getRateOfCurrency(string toCurrency, Envelope envelope)
        {
            log.Info("getRateOfCurrency(string toCurrency): Started!");
            foreach (CubeCubeCube currency in envelope.Cube.Cube1.Cube)
            {
                if (currency.currency.Equals(toCurrency))
                {
                    return currency.rate;
                }
            }
            log.Info("getRateOfCurrency(string toCurrency): Ended!");
            return -1;
        }

        /// <summary>
        /// This method gets all the currencies and return only a list with the currency names.
        /// </summary>
        /// <returns></returns>
        public CurrencyDTO getCurrencyNames()
        {
            log.Info("getCurrencyNames(): Started!");
            envelope = getEnvelope();
            // CurrencyDTO object that will contain all the available currency names
            CurrencyDTO currencyDTO = new CurrencyDTO();
            // populate the list
            currencyDTO.names = getNamesFromEnvelope(envelope);

            log.Info("getCurrencyNames(): Ended!");
            return currencyDTO;
        }

        /// <summary>
        /// Returns a list with all the names form the given envelope
        /// </summary>
        /// <param name="envelope"></param>
        /// <returns></returns>
        public List<string> getNamesFromEnvelope(Envelope envelope)
        {
            log.Info("getNamesFromEnvelope(Envelope envelope): Started!");
            List<string> names = new List<string>();
            foreach (CubeCubeCube currency in envelope.Cube.Cube1.Cube)
            {
                log.Info("getCurrencyNames(): Currency with Name: " + currency.currency + " added to the list!");
                names.Add(currency.currency);
            }
            log.Info("getNamesFromEnvelope(Envelope envelope): Ended!");
            return names;
        }

        public double convertCurrencies(string fromCurrency, string toCurrency, double amount)
        {
            return convertCurrencies(fromCurrency, toCurrency, amount, getEnvelope());
        }

        /// <summary>
        /// In this method the convertion from one currency into another one is performed.
        /// Then the result is mutliplied by the amount. 
        /// If the amount is less then 0, than we return 0.0.
        /// </summary>
        /// <param name="fromCurrency"></param>
        /// <param name="toCurrency"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public double convertCurrencies(string fromCurrency, string toCurrency, double amount, Envelope envelope)
        {
            log.Info("convertCurrencies(string fromCurrency, string toCurrency, double amount): Started!");
            if (amount < 0)
            {
                return 0.0;
            }

            double total = -1;

            // If we want to convert from EUR, then we perform normal converting.
            if (fromCurrency.Equals("EUR"))
            {
                log.Info("convertCurrencies(string fromCurrency, string toCurrency, double amount): Getting the rate for: " + toCurrency);
                // Find the rate of the currency to convert to
                double rate = getRateOfCurrency(toCurrency, envelope);
                if (rate != -1)
                {
                    log.Info("convertCurrencies(string fromCurrency, string toCurrency, double amount): Calculating: " + amount + " " + fromCurrency + " to: " + toCurrency);
                    // if the currency was found, perform the convertion
                    total = amount * rate;
                }
            }
            else
            {
                // get the rate of the currencies to EUR
                log.Info("convertCurrencies(string fromCurrency, string toCurrency, double amount): Getting the rate for: " + toCurrency);
                double toCurrencyRate = getRateOfCurrency(toCurrency, envelope);

                log.Info("convertCurrencies(string fromCurrency, string toCurrency, double amount): Getting the rate for: " + fromCurrency);
                double fromCurrencyRate = getRateOfCurrency(fromCurrency, envelope);

                log.Info("convertCurrencies(string fromCurrency, string toCurrency, double amount): Calculating: " + amount + " " + fromCurrency + " to: " + toCurrency);
                total = convertCrossCurrencies(toCurrencyRate, fromCurrencyRate, amount);
            }

            log.Info("convertCurrencies(string fromCurrency, string toCurrency, double amount): Ended!");
            return total;
        }
    }
}