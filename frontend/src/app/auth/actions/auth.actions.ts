import {createAction, props} from '@ngrx/store';
import {User} from '../models/user.model';
import {Currency} from "../../shared/models/currency.model";

export enum AuthActionTypes {
  Register = '[RegisterComponent] Register',
  RegisterSuccess = '[API] RegisterSuccess',
  RegisterFailure = '[API] RegisterFailure',

  GetCurrencies = '[RegisterComponent] GetCurrencies',
  GetCurrenciesSuccess = '[API] GetCurrenciesSuccess',
  GetCurrenciesFailure = '[API] GetCurrenciesFailure',
}

export const Register = createAction(
  AuthActionTypes.Register,
  props<{ user: User }>()
);
export const RegisterSuccess = createAction(
  AuthActionTypes.RegisterSuccess,
);
export const RegisterFailure = createAction(
  AuthActionTypes.RegisterFailure,
);

export const GetCurrencies = createAction(
  AuthActionTypes.GetCurrencies,
);
export const GetCurrenciesSuccess = createAction(
  AuthActionTypes.GetCurrenciesSuccess,
  props<{ currency: Currency }>(),
);
export const GetCurrenciesFailure = createAction(
  AuthActionTypes.GetCurrenciesFailure,
);
