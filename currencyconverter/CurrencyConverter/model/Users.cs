﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CurrencyConverter.model
{
    public class Users
    {
        [Key]
        public string username { get; set; }
        public string password { get; set; }
        public bool enabled { get; set; }
    }
}