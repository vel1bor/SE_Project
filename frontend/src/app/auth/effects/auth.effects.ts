import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {AuthService} from '../services/auth.service';
import {
  GetCurrencies,
  GetCurrenciesFailure,
  GetCurrenciesSuccess,
  Register,
  RegisterFailure,
  RegisterSuccess
} from '../actions/auth.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {Router} from '@angular/router';
import {Currency} from '../../shared/models/currency.model';

@Injectable({
  providedIn: 'root'
})
export class AuthEffects {

  onRegister$ = createEffect(() => this.actions$
    .pipe(
      ofType(Register),
      switchMap(action => {
        return this.authService
          .register(action.user)
          .pipe(
            map(() => {
              return RegisterSuccess();
            }),
            catchError(reason => {
              return of(RegisterFailure());
            })
          );
      })
    )
  );

  onGetCurrency$ = createEffect(() => this.actions$
    .pipe(
      ofType(GetCurrencies),
      switchMap(action => {
        return this.authService
          .getCurrencies()
          .pipe(
            map((currency: Currency) => {
              return GetCurrenciesSuccess({currency});
            }),
            catchError(reason => {
              return of(GetCurrenciesFailure());
            })
          );
      })
    )
  );

  onRegisterSuccess$ = createEffect(() => this.actions$
    .pipe(
      ofType(RegisterSuccess),
      map(() => {
        this.router.navigateByUrl('/auth');
      })
    ), {dispatch: false}
  );

  constructor(
    private router: Router,
    private actions$: Actions,
    private authService: AuthService,
  ) {
  }
}
