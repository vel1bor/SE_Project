import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, shareReplay} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {User} from '../models/user.model';
import {Currency} from "../../shared/models/currency.model";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private commonHttpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    .set('Access-Control-Expose-Headers', 'Access-Control-Allow-Origin')
    .set('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE')
    .set('Access-Control-Allow-Origin', '*');

  constructor(
    private http: HttpClient,
  ) {
  }

  register(user: User) {
    return this.http.post(
      `http://ec2-18-157-185-120.eu-central-1.compute.amazonaws.com:8080/api/user/register`,
      user,
      {
        headers: this.commonHttpHeaders,
      }
    ).pipe(
      catchError(error => {
        return throwError(error);
      }),
      shareReplay(1),
    );
  }

  getCurrencies(): Observable<Currency> {
    return this.http.get<Currency>(
      `http://ec2-18-157-185-120.eu-central-1.compute.amazonaws.com:8080/api/currency`,
      {
        headers: this.commonHttpHeaders,
      }
    ).pipe(
      catchError(error => {
        return throwError(error);
      }),
      shareReplay(1),
    );
  }
}
