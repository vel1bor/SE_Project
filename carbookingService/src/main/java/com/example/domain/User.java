package com.example.domain;


import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity(name = "users")
public class User {

    @Id
    @Column(length = 20, nullable = false)
    private String username;

    @Column(length = 20, nullable = false)
    private String password;

    @Column(nullable = false)
    private boolean enabled = false;

    @Column(length = 3)
    private String currency;


    @OneToMany(
            fetch = FetchType.EAGER,
            mappedBy = "user")
    List<Booking> bookings;

    public List<Booking> getBookings() {
        if (bookings == null) {
            bookings = new ArrayList<>();
        }
        return bookings;
    }

}
