package com.example.utils;

import com.example.domain.Booking;
import com.example.domain.Car;
import com.example.resource.dtos.RabbitUserDto;
import com.example.services.BookingService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

//@RabbitListener(queues = "bookings")
public class RabbitMQReceiverBooking {

    @Autowired
    private BookingService bookingService;




    @RabbitListener(queues = "bookings")
    public Booking receive2(List<Booking> bookings) throws InterruptedException {
        return receive(bookings);
    }

    public Booking receive(List<Booking> bookings) {
        System.out.println("Received");
        return bookingService.getLatestBooking(bookings);
    }

}