package com.example.resource.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class BookingDto implements Serializable {
    private String id;

    @NotNull
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String carId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDate startDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDate endDate;

    private CarDto car;

    private BigDecimal price;
}
