﻿using CurrencyConverter.context;
using CurrencyConverter.model;
using CurrencyConverter.repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyConverter.service
{
    public class UserService : UserRepository
    {
        public bool findUserByUsernameAndPassword(string username, string password)
        {
            using (var db = new UserContext())
            {
                Users user = db.users
                    .Where(u => u.username.Equals(username) && u.password.Equals(password) && u.enabled == true)
                    .FirstOrDefault();

                if (user != null)
                {
                    return true;
                }
            }
            return false;
        }
    }
}