package com.example.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringCloudConfig {
    @Value( "${routes.authmodule}")
    private String authModuleURL;
    @Value( "${routes.usermodule}")
    private String userModuleURL;
    @Value( "${routes.carsmodule}")
    private String carsModuleURL;
    @Value( "${routes.bookingmodule}")
    private String bookingModuleURL;

    Logger logger = LoggerFactory.getLogger(SpringCloudConfig.class);
    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        logger.info(bookingModuleURL);
        logger.info(carsModuleURL);
        logger.info(userModuleURL);
        logger.info(authModuleURL);

        return builder.routes()
                .route(r -> r
                        .path("/api/user/**")
                        .uri(userModuleURL)
                        .id("userModule"))
                .route(r -> r
                        .path("/api/currency")
                        .uri(carsModuleURL)
                        .id("currencyModule"))

                .route(r -> r
                        .path("/authenticate")
                        .uri(authModuleURL)
                        .id("authModule"))

                .route(r -> r
                        .path("/api/cars/**")
                        .uri(carsModuleURL)
                        .id("carsModule"))

                .route(r -> r
                        .path("/api/booking/**")
                        .uri(bookingModuleURL)
                        .id("bookingModule"))
                .build();
    }

}
