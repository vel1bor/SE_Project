import {createReducer, createSelector, on} from '@ngrx/store';
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {Car} from '../../shared/models/car.model';
import {
  BookCarSuccess,
  DeselectCar,
  GetAvailableCarsSuccess,
  ReturnCarSuccess,
  SelectCar
} from '../actions/rental.actions';

export interface RentalState extends EntityState<Car> {
  selectedCar: Car;
}

export const adapter: EntityAdapter<Car> = createEntityAdapter<Car>({
  selectId: e => e.id,
  sortComparer: (a, b) => a.id - b.id,
});

export const initialState: RentalState = adapter.getInitialState({
  selectedCar: null,
});

const privateRentalReducer = createReducer(
  initialState,
  on(GetAvailableCarsSuccess, (state, action) => adapter.addMany(action.payload.cars, state)),
  on(BookCarSuccess, (state, action) => adapter.removeOne(action.booking.carId, state)),
  on(ReturnCarSuccess, (state, action) => adapter.updateOne({
    id: action.car.id,
    changes: action.car,
  }, state)),
  on(SelectCar, (state, action) => ({
    ...state,
    selectedCar: action.car,
  })),
  on(DeselectCar, (state) => ({
    ...state,
    selectedCar: undefined,
  }))
);

export function rentalReducer(state, action) {
  return privateRentalReducer(state, action);
}

const {
  selectAll
} = adapter.getSelectors();

export const getBookedCars = createSelector(
  selectAll,
  cars => cars.filter(c => c.booked)
);
export const getAvailableCars = createSelector(
  selectAll,
  cars => cars.filter(c => !c.booked)
);
export const getSelectedCar = (state: RentalState) => state.selectedCar;
