import {Component, OnInit} from '@angular/core';
import {BookingFacade} from '../../services/facades/booking.facade';
import {Booking} from '../../../shared/models/booking.model';

@Component({
  selector: 'app-booking-list',
  templateUrl: './booking-list.component.html',
  styleUrls: ['./booking-list.component.css']
})
export class BookingListComponent implements OnInit {

  bookings$;

  constructor(
    private facade: BookingFacade,
  ) {
  }

  ngOnInit() {
    this.bookings$ = this.facade.getBookings();
  }

  onBookingStopped(booking: Booking) {
    this.facade.stopBooking(booking);
  }
}
