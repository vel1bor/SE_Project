import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterDumbComponent } from './register-dumb.component';

describe('RegisterDumbComponent', () => {
  let component: RegisterDumbComponent;
  let fixture: ComponentFixture<RegisterDumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterDumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
