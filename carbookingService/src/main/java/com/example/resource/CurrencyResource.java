package com.example.resource;


import com.example.services.CurrencyConverterService;
import com.example.soap.ConvertCurrenciesResponse;
import com.example.soap.ConverterDTO;
import com.example.soap.CurrencyDTO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.example.resource.BaseResource.CURRENCY;

@Api()
@RestController()
@RequestMapping(CURRENCY)
public class CurrencyResource extends BaseResource {

    private final CurrencyConverterService service;

    @Autowired
    public CurrencyResource(CurrencyConverterService service) {
        this.service = service;
    }

    @GetMapping()
    public CurrencyDTO getAllCurrencies() {
        return service.getAllCurrencies().getGetCurrencyNamesResult();
    }

    @GetMapping(value = "/convert")
    public ConverterDTO convertTo(@RequestParam String from, String to, Double amount) {
        ConvertCurrenciesResponse response = service.convertFromToAmount(from, to, amount);
        return response.getConvertCurrenciesResult();
    }
}
