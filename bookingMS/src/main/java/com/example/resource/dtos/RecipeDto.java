package com.example.resource.dtos;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class RecipeDto implements Serializable {

    private BigDecimal totalCost;

    private String currency;
}
