package com.example.services;

import com.example.domain.Booking;
import com.example.domain.Car;
import com.example.repos.BookingRepo;
import com.example.resource.dtos.BookingDto;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;


@Service
public class BookingService {

    private BookingRepo bookingRepo;

    public BookingService(@Autowired final BookingRepo bookingRepo) {
        this.bookingRepo = bookingRepo;
    }

    @Transactional
    public Booking createBooking(Booking booking) {
        Car car = booking.getCar();
        Booking latestBooking = getLatestBooking(car.getBookings());
        if (latestBooking != null && isBooked(latestBooking)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Car is already booked by other User");
        }
        booking.setStartDate(LocalDate.now());
        return bookingRepo.save(booking);
    }

    @Transactional(readOnly = true)
    public Booking getBookingById(Long id) {
        return bookingRepo
                .findById(id)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "Booking does not exist"
                ));
    }

    boolean isBooked(Booking booking) {
        return booking.getEndDate() == null;
    }

    Booking getLatestBooking(List<Booking> bookings) {
        if (bookings.isEmpty()) {
            return null;
        } else if (bookings.size() > 1) {
            return bookings.stream().max(Comparator.comparing(Booking::getStartDate)).orElse(null);
        } else {
            return bookings.get(0);
        }
    }

    public List<Booking> getBookingsForUser(String username) {
        return bookingRepo.findAllByUsername(username);
    }

    public BigDecimal calculateTotalCost(Booking booking) {
        BigDecimal price = booking.getCar().getPricePerDay();
        long rentedDays = DAYS.between(booking.getStartDate(), booking.getEndDate());
        return price.multiply(BigDecimal.valueOf(rentedDays));
    }

    @Transactional
    public Booking updateBooking(BookingDto dto, String username) {
        Booking booking = getBookingById(dto.getId());
        // Check if the updating user is the user original created the booking
        if (!booking.getUser().getUsername().equals(username)) {
            throw new ServiceException("This user cannot update this booking");
        }
        booking.setEndDate(LocalDate.now().plusDays(1));
        booking.setPrice(calculateTotalCost(booking));
        return bookingRepo.save(booking);
    }
}
