package com.example.config;

import com.example.utils.RabbitMQSenderCars;
import com.example.utils.RabbitMQReceiverCars;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Bean
    public DirectExchange direct() {
        return new DirectExchange("tut.direct");
    }

    @Bean
    public Queue cars() {
        return new Queue("cars");
    }

    @Bean
    public RabbitMQReceiverCars receiver() {
        return new RabbitMQReceiverCars();
    }

    @Bean
    public RabbitMQSenderCars sender() {
        return new RabbitMQSenderCars();
    }

    @Bean
    public Binding bindingUsers(DirectExchange direct,
                                Queue cars) {
        return BindingBuilder.bind(cars)
                .to(direct)
                .with("cars");
    }
}
