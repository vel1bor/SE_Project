import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, flatMap, map, switchMap} from 'rxjs/operators';
import {Booking} from '../../shared/models/booking.model';
import {of} from 'rxjs';
import {
  GetBookings,
  GetBookingsFailure,
  GetBookingsSuccess,
  StopBooking,
  StopBookingFailure,
  StopBookingSuccess
} from '../actions/booking.actions';
import {BookingService} from '../services/booking.service';

@Injectable({
  providedIn: 'root'
})
export class BookingEffects {

  onGetBookings$ = createEffect(() => this.actions$
    .pipe(
      ofType(GetBookings),
      switchMap(() => {
        return this.bookingService
          .getBookings()
          .pipe(
            map((bookings: Booking[]) => {
              return GetBookingsSuccess({payload: {bookings}});
            }),
            catchError(() => {
              return of(GetBookingsFailure());
            })
          );
      }),
    )
  );

  onStopBooking$ = createEffect(() => this.actions$
    .pipe(
      ofType(StopBooking),
      switchMap((action) => {
        return this.bookingService
          .stopBooking(action.booking)
          .pipe(
            map((booking: Booking) => {
              return StopBookingSuccess({booking});
            }),
            catchError(() => {
              return of(StopBookingFailure());
            })
          );
      }),
    )
  );

  constructor(
    private actions$: Actions,
    private bookingService: BookingService,
  ) {
  }
}
