import {createReducer, on} from '@ngrx/store';
import {GetCurrenciesSuccess} from '../actions/auth.actions';

export interface AuthState {
  currencies: string[];
}

export const initialState: AuthState = {
  currencies: [],
};

const privateAuthReducer = createReducer(
  initialState,
  on(GetCurrenciesSuccess, (state, action) => ({...state, currencies: action.currency.names.string})),
);

export function authReducer(state, action) {
  return privateAuthReducer(state, action);
}

export const getCurrencies = (state: AuthState) => state.currencies;
