package com.example.resource;

import com.example.domain.Car;
import com.example.resource.dtos.CarDto;
import com.example.services.CarService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.resource.BaseResource.CARS;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Api()
@RestController()
@RequestMapping(CARS)
public class CarResource extends BaseResource {

    private CarService carService;

    @Autowired
    public CarResource(final CarService carService) {
        this.carService = carService;
    }

    // Get list of all cars from database
    @GetMapping()
    public ResponseEntity<List<CarDto>> getAllCars(@RequestParam boolean onlyAvailable) {
        List<Car> carsDomain = onlyAvailable ? carService.findAllAvaliable() : carService.findAll();
        List<CarDto> cars = carsDomain.stream().map(car -> mapper.convertToDto(car, carService.isCarBooked(car))).collect(Collectors.toList());
        return ResponseEntity.ok(cars);
    }

    // Create a new car. Syntax: "localhost:8080/cars/create" and pass Car as JSON object
    @PostMapping()
    public ResponseEntity<CarDto> createCar(@RequestBody CarDto carDto) {
        if (carDto.getId() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID already set");
        }

        Car car = mapper.convertToDomain(carDto);
        CarDto createdCar = mapper.convertToDto(carService.createCar(car), false);
        return ResponseEntity.ok(createdCar);
    }

}
