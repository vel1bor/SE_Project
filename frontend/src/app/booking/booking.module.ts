import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {BookingRoutingModule} from './booking-routing.module';
import {BookingListComponent} from './containers/booking-list/booking-list.component';
import {BookingListDumbComponent} from './components/booking-list-dumb/booking-list-dumb.component';
import {StoreModule} from '@ngrx/store';
import {bookingReducer} from './reducers/booking.reducer';
import {EffectsModule} from '@ngrx/effects';
import {BookingEffects} from './effects/booking.effects';

@NgModule({
  declarations: [
    BookingListComponent,
    BookingListDumbComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    BookingRoutingModule,
    StoreModule.forFeature('bookings', bookingReducer),
    EffectsModule.forFeature([BookingEffects])
  ]
})
export class BookingModule {
}
