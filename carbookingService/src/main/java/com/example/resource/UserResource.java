package com.example.resource;

import com.example.domain.User;
import com.example.resource.dtos.UserDto;
import com.example.services.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.example.resource.BaseResource.USER;

@Api
@RestController
@RequestMapping(USER)
public class UserResource extends BaseResource {

    private final UserService userService;

    @Autowired
    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/register")
    public ResponseEntity<UserDto> register(@RequestBody UserDto userDto) {
        User user = mapper.convertToDomain(userDto);
        userService.register(user);
        UserDto createdUser = mapper.convertToDto(user);
        return ResponseEntity.ok(createdUser);
    }
}
