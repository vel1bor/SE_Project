﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using CurrencyConverter.model;
using CurrencyConverter.service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CurrencyConverterTest
{
    [TestClass]
    public class CurrencyConverterServiceTest
    {
        [TestMethod]
        public void testConvertCurrenciesFrom1EURToUSD()
        {
            CurrencyConverterService currencyConverterService = new CurrencyConverterService();
            Envelope envelope = generateTestEnvelope();
            Assert.AreEqual(1.086, currencyConverterService.convertCurrencies("EUR", "USD", 1, envelope));
        }

        [TestMethod]
        public void testConvertCrossCurrenciesFrom1USDToJPY()
        {
            CurrencyConverterService currencyConverterService = new CurrencyConverterService();
            Envelope envelope = generateTestEnvelope();
            Assert.AreEqual(107.60589318600367, currencyConverterService.convertCurrencies("USD", "JPY", 1, envelope));
        }

        [TestMethod]
        public void testConvertCrossCurrenciesFrom1USDToUSD()
        {
            CurrencyConverterService currencyConverterService = new CurrencyConverterService();
            Envelope envelope = generateTestEnvelope();
            Assert.AreEqual(1, currencyConverterService.convertCurrencies("USD", "USD", 1, envelope));
        }

        [TestMethod]
        public void testgetNamesFromEnvelope()
        {
            CurrencyConverterService currencyConverterService = new CurrencyConverterService();
            Envelope envelope = generateTestEnvelope();
            Assert.AreEqual(3, currencyConverterService.getNamesFromEnvelope(envelope).Count);
        }

        [TestMethod]
        public void testgetRateOfCurrencyUSD()
        {
            CurrencyConverterService currencyConverterService = new CurrencyConverterService();
            Envelope envelope = generateTestEnvelope();
            Assert.AreEqual(1.0860, currencyConverterService.getRateOfCurrency("USD", envelope));
        }

        [TestMethod]
        public void testgetRateOfNotExistingCurrency()
        {
            CurrencyConverterService currencyConverterService = new CurrencyConverterService();
            Envelope envelope = generateTestEnvelope();
            Assert.AreEqual(-1, currencyConverterService.getRateOfCurrency("AAA", envelope));
        }

        /// <summary>
        /// This method can be used to generate the test object of the class Envelope.
        /// </summary>
        /// <returns></returns>
        private Envelope generateTestEnvelope()
        {
            Envelope envelope = new Envelope();
            envelope.Cube = new Cube();
            envelope.Cube.Cube1 = new CubeCube();
            envelope.Cube.Cube1.Cube = new CubeCubeCube[3];

            CubeCubeCube cube = new CubeCubeCube();
            cube.currency = "USD";
            cube.rate = 1.0860;
            envelope.Cube.Cube1.Cube[0] = cube;

            cube = new CubeCubeCube();
            cube.currency = "JPY";
            cube.rate = 116.86;
            envelope.Cube.Cube1.Cube[1] = cube;

            cube = new CubeCubeCube();
            cube.currency = "BGN";
            cube.rate = 1.9558;
            envelope.Cube.Cube1.Cube[2] = cube;

            return envelope;
        }
    }
}
