package com.example.utils;

import com.example.domain.Car;
import com.example.domain.User;
import com.example.resource.dtos.RabbitUserDto;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

public class RabbitMQSenderBooking {

    @Autowired
    private DirectExchange direct;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public Car findCarById(String carId) {
        return (Car) this.rabbitTemplate.convertSendAndReceive(direct.getName(), "cars", carId);
    }

    public RabbitUserDto findUserByUsername(String username, String password) {
        return (RabbitUserDto) this.rabbitTemplate.convertSendAndReceive(direct.getName(), "users", new RabbitUserDto(username, password));
    }

    public RabbitUserDto findUserByUsername(String username) {
        return (RabbitUserDto) this.rabbitTemplate.convertSendAndReceive(direct.getName(), "users", new RabbitUserDto(username));
    }
}
