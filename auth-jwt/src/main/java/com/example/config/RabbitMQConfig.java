package com.example.config;

import com.example.util.RabbitMQReceiverUsers;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Bean
    public DirectExchange direct() {
        return new DirectExchange("tut.direct");
    }

    @Bean
    public Queue users() {
        return new Queue("users");
    }

    @Bean
    public RabbitMQReceiverUsers receiver() {
        return new RabbitMQReceiverUsers();
    }

    @Bean
    public Binding bindingUsers(DirectExchange direct,
                             Queue users) {
        return BindingBuilder.bind(users)
                .to(direct)
                .with("users");
    }
}
