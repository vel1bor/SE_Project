import {Component, OnInit} from '@angular/core';
import {AuthFacade} from '../../services/facades/auth.facade';
import {User} from '../../models/user.model';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  currencies$: Observable<string[]>;

  constructor(
    private facade: AuthFacade,
  ) {
  }

  ngOnInit() {
    this.currencies$ = this.facade.getCurrencies();
  }

  onRegister(user: User) {
    this.facade.register(user);
  }
}
