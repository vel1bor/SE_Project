export interface Currency {
  names: ArrayOfString;
}

export interface ArrayOfString {
  string: string[];
}
