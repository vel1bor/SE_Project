export interface User {
  username: string;
  password: string;
  currency: string;
}
