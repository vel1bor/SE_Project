package com.example.resource.dtos;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class RecipeDto {

    private BigDecimal totalCost;

    private String currency;
}
