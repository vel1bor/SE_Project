package com.example.repos;

import com.example.domain.Booking;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookingRepo extends CrudRepository<Booking, Long> {
    @Override
    List<Booking> findAll();

    @Query("SELECT b FROM Booking b WHERE b.user.username = :username")
    List<Booking> findAllByUsername(String username);
}
