import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Booking} from '../../../shared/models/booking.model';

@Component({
  selector: 'app-booking-list-dumb',
  templateUrl: './booking-list-dumb.component.html',
  styleUrls: ['./booking-list-dumb.component.css']
})
export class BookingListDumbComponent implements OnInit {

  @Input() bookings: Booking[];
  @Output() bookingStopped = new EventEmitter<Booking>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onStopClicked(booking: Booking) {
    this.bookingStopped.emit(booking);
  }
}
