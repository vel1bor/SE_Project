package com.example.services;

import com.example.domain.Booking;
import com.example.domain.Car;
import com.example.repos.CarRepo;
import com.example.repos.UserRepo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
public class CarService {

    private CarRepo carRepo;
    private UserRepo userRepo;
    private BookingService bookingService;
    private CurrencyConverterService ccService;

    @Autowired
    public CarService(final CarRepo carRepo, final BookingService bookingService, final UserRepo userRepo, final CurrencyConverterService ccService) {
        this.bookingService = bookingService;
        this.carRepo = carRepo;
        this.userRepo = userRepo;
        this.ccService = ccService;
    }

    @Transactional(readOnly = true)
    public List<Car> findAllAvaliable() {
        return convertPrice(carRepo.findAll()).stream().filter(car -> !isCarBooked(car)).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<Car> findAll() {
        return convertPrice(carRepo.findAll());
    }

    @Transactional(readOnly = true)
    public Car getCarById(Long id) {
        return carRepo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "Car with this id does not exist"
                ));
    }

    @Transactional
    public Car createCar(Car car) {
        return carRepo.save(car);
    }

    @Transactional
    public Car updateCar(Car car) {
        if (!carRepo.existsById(car.getId())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Car with this id does not exist");
        }
        return carRepo.save(car);
    }

    @Transactional
    public void deleteCar(Long id) {
        if (!carRepo.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Car with this id does not exist");
        }
        carRepo.deleteById(id);
    }

    public boolean isCarBooked(Car car) {
        Booking latestBooking = bookingService.getLatestBooking(car.getBookings());
        return latestBooking != null && bookingService.isBooked(latestBooking);
    }

    public String getCurrency() {
        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String currency = userRepo.findByUsername(currentUser).get().getCurrency();
        if (currency == null) {
            currency = "USD";
        }
        return currency;
    }

    public List<Car> convertPrice(List<Car> cars) {
        String toCurrency = getCurrency();
        if (!toCurrency.equals("USD")) {
            Double conversionRateDoubleValue = ccService.convertFromToAmount("USD", toCurrency, 1.00).getConvertCurrenciesResult().getResult();
            BigDecimal conversionRate = new BigDecimal(conversionRateDoubleValue, MathContext.DECIMAL64).multiply(new BigDecimal(-1, MathContext.DECIMAL64));
            for (Car car : cars) {
                BigDecimal convertedPrice = car.getPricePerDay().multiply(conversionRate).round(new MathContext(2, RoundingMode.HALF_UP));
                if (convertedPrice.compareTo(BigDecimal.ZERO) < 0) {
                    convertedPrice = convertedPrice.multiply(new BigDecimal(-1, MathContext.DECIMAL64).round(new MathContext(2, RoundingMode.HALF_UP)));
                }
                car.setPricePerDay(convertedPrice);
            }
            log.info("All prices converted from USD into" + toCurrency);
        } else {
            log.info("No currency conversion needed");
        }
        return cars;
    }

    public Car convertPrice(Car car) {
        String toCurrency = getCurrency();
        if (!toCurrency.equals("USD")) {
            Double conversionRateDoubleValue = ccService.convertFromToAmount("USD", toCurrency, 1.00).getConvertCurrenciesResult().getResult();
            BigDecimal conversionRate = new BigDecimal(conversionRateDoubleValue, MathContext.DECIMAL64).multiply(new BigDecimal(-1, MathContext.DECIMAL64));
            BigDecimal convertedPrice = car.getPricePerDay().multiply(conversionRate).round(new MathContext(2, RoundingMode.HALF_UP));
            if (convertedPrice.compareTo(BigDecimal.ZERO) < 0) {
                convertedPrice = convertedPrice.multiply(new BigDecimal(-1, MathContext.DECIMAL64).round(new MathContext(2, RoundingMode.HALF_UP)));
            }
            car.setPricePerDay(convertedPrice);
            log.info("All prices converted from USD into " + toCurrency + "");
        } else {
            log.info("No currency conversion needed");
        }

        return car;
    }

}
