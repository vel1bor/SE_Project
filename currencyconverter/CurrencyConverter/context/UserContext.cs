﻿using CurrencyConverter.model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyConverter.context
{
    public class UserContext : DbContext
    {
        public virtual DbSet<Users> users { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(@"Host=database-1.ct0dkb3jdafv.eu-central-1.rds.amazonaws.com;Database=se_db;Username=postgres;Password=postgres123");
        }
    }
}