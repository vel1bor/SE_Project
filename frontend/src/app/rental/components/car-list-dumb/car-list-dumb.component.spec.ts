import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarListDumbComponent } from './car-list-dumb.component';

describe('CarListDumbComponent', () => {
  let component: CarListDumbComponent;
  let fixture: ComponentFixture<CarListDumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarListDumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarListDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
