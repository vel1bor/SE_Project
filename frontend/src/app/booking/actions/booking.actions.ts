import {createAction, props} from '@ngrx/store';
import {Booking} from '../../shared/models/booking.model';

export enum BookingActionTypes {
  GetBookings = '[BookingListComponent] GetBookings',
  GetBookingsSuccess = '[API] GetBookingsSuccess',
  GetBookingsFailure = '[API] GetBookingsSuccess',

  StopBooking = '[BookingListComponent] StopBooking',
  StopBookingSuccess = '[API] StopBookingSuccess',
  StopBookingFailure = '[API] StopBookingSuccess',
}

export const GetBookings = createAction(
  BookingActionTypes.GetBookings,
);
export const GetBookingsSuccess = createAction(
  BookingActionTypes.GetBookingsSuccess,
  props<{ payload: { bookings: Booking[] } }>()
);
export const GetBookingsFailure = createAction(
  BookingActionTypes.GetBookingsFailure,
);

export const StopBooking = createAction(
  BookingActionTypes.StopBooking,
  props<{ booking: Booking }>()
);
export const StopBookingSuccess = createAction(
  BookingActionTypes.StopBookingSuccess,
  props<{ booking: Booking }>()
);
export const StopBookingFailure = createAction(
  BookingActionTypes.StopBookingFailure,
);
