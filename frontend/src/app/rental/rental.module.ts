import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {RentalRoutingModule} from './rental-routing.module';
import {CarListComponent} from './containers/car-list/car-list.component';
import {CarListDumbComponent} from './components/car-list-dumb/car-list-dumb.component';
import {StoreModule} from '@ngrx/store';
import {rentalReducer} from './reducers/rental.reducer';
import {EffectsModule} from '@ngrx/effects';
import {RentalEffects} from './effects/rental.effects';
import {GoogleMapsModule} from '@angular/google-maps';
import {InnerCarListComponent} from './components/car-list-dumb/inner-car-list/inner-car-list.component';

@NgModule({
  declarations: [
    CarListComponent,
    CarListDumbComponent,
    InnerCarListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RentalRoutingModule,
    StoreModule.forFeature('rental', rentalReducer),
    EffectsModule.forFeature([RentalEffects]),
    GoogleMapsModule,
  ]
})
export class RentalModule {
}
