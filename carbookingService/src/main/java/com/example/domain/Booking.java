package com.example.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "car_id")
    private Car car;

    @ManyToOne()
    @JoinColumn(name = "user_id")
    private User user;

    @Column(precision = 7, scale = 2)
    private BigDecimal price;

    @Column
    private LocalDate startDate;

    @Column
    private LocalDate endDate;
}
