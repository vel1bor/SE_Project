import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Booking} from '../../shared/models/booking.model';
import {catchError, shareReplay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  private commonHttpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    .set('Access-Control-Expose-Headers', 'Access-Control-Allow-Origin')
    .set('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE')
    .set('Access-Control-Allow-Origin', '*');

  constructor(
    private http: HttpClient,
  ) {
  }

  getBookings(): Observable<Booking[]> {
    return this.http.get<Booking[]>(
      `http://ec2-18-157-185-120.eu-central-1.compute.amazonaws.com:8080/api/booking`,
      {
        headers: this.commonHttpHeaders,
      }
    ).pipe(
      catchError(error => {
        return throwError(error);
      }),
      shareReplay(1),
    );
  }

  stopBooking(booking: Booking): Observable<Booking> {
    return this.http.put<Booking>(
      `http://ec2-18-157-185-120.eu-central-1.compute.amazonaws.com:8080/api/booking/closing`,
      booking,
      {
        headers: this.commonHttpHeaders,
      }
    ).pipe(
      catchError(error => {
        return throwError(error);
      }),
      shareReplay(1),
    );
  }
}
