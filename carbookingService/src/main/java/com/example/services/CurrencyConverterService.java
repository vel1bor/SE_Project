package com.example.services;


import com.example.soap.*;
import com.example.utils.SoapClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class CurrencyConverterService {

    private SoapClient soapClient;

    public CurrencyConverterService(@Autowired final SoapClient soapClient) {
        this.soapClient = soapClient;
    }

    @Transactional(readOnly = true)
    public GetCurrencyNamesResponse getAllCurrencies() {
        ObjectFactory objectFactory = new ObjectFactory();

        GetCurrencyNames cNames = objectFactory.createGetCurrencyNames();
        GetCurrencyNamesResponse response = soapClient.getCurrencies("https://currencyconverter20200418192538.azurewebsites.net/webservice/CurrencyConverterWebservice.asmx?op=getCurrencyNames", cNames);

        return response;
    }

    @Transactional(readOnly = true)
    public ConvertCurrenciesResponse convertFromToAmount(String from, String to, Double amount) {
        ObjectFactory objectFactory = new ObjectFactory();
        ConvertCurrencies cc = objectFactory.createConvertCurrencies();
        cc.setFromCurrency(from);
        cc.setToCurrency(to);
        cc.setAmount(amount);

        ConvertCurrenciesResponse response = soapClient.convertCurrency("https://currencyconverter20200418192538.azurewebsites.net/webservice/CurrencyConverterWebservice.asmx?op=convertCurrencies", cc);

        return response;
    }
}
