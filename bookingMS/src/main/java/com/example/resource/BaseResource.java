package com.example.resource;

import com.example.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@EnableGlobalMethodSecurity(prePostEnabled = true)
public abstract class BaseResource {

    @Autowired
    protected Mapper mapper;

    protected static final String PREFIX = "/api";
    protected static final String BOOKING = PREFIX + "/booking";
}
