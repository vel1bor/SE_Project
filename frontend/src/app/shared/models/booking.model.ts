import {Car} from './car.model';

export interface Booking {
  car: Car;
  id: number;
  carId: number;
  endDate: string;
  startDate: string;
  price: number;
}
