package com.example.utils;

import com.example.domain.Car;
import com.example.domain.User;
import com.example.resource.dtos.CarDto;
import com.example.resource.dtos.UserDto;
import com.example.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class Mapper {

    private CarService carService;

    @Autowired
    public Mapper(final CarService carService
    ) {
        this.carService = carService;
    }


    public CarDto convertToDto(Car car, boolean isBooked) {
        CarDto carDto = convertToDto(car);
        carDto.setBooked(isBooked);
        return carDto;
    }


    public CarDto convertToDto(Car car) {
        CarDto carDto = new CarDto();
        carDto.setId(car.getId());
        carDto.setName(car.getName());
        carDto.setPrice(car.getPricePerDay());
        carDto.setLatitude(car.getLatitude());
        carDto.setLongitude(car.getLongitude());
        carDto.setLicencePlate(car.getLicencePlate());
        return carDto;
    }


    public Car convertToDomain(CarDto carDto) {
        Car car = new Car();
        car.setId(carDto.getId());
        car.setName(carDto.getName());
        car.setPricePerDay(carDto.getPrice());
        car.setLatitude(carDto.getLatitude());
        car.setLongitude(carDto.getLongitude());
        return car;
    }



}
