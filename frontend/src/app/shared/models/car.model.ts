export interface Car {
  id: number;
  name: string;
  licencePlate: string;
  price: number;
  latitude: number;
  longitude: number;
  booked: boolean;
}
