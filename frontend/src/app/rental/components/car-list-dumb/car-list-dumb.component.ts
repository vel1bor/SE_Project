import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Car} from '../../../shared/models/car.model';

@Component({
  selector: 'app-car-list-dumb',
  templateUrl: './car-list-dumb.component.html',
  styleUrls: ['./car-list-dumb.component.css']
})
export class CarListDumbComponent implements OnInit {

  @Input() bookedCars: Car[];
  @Input() availableCars: Car[];
  @Input() selectedCar: Car;
  @Output() carBooked: EventEmitter<number> = new EventEmitter<number>();
  @Output() carReturned: EventEmitter<number> = new EventEmitter<number>();
  @Output() carSelected: EventEmitter<Car> = new EventEmitter<Car>();
  @ViewChild('map') map: google.maps.Map;
  center: google.maps.LatLngLiteral;
  options: google.maps.MapOptions;

  constructor() {
  }

  ngOnInit() {
    this.center = {
      lat: 48.235330,
      lng: 16.411839,
    };
    this.options = {
      zoomControl: false,
      fullscreenControl: false,
      streetViewControl: false,
      mapTypeControl: false,
    };
  }

  onCarBooked(carId: number) {
    this.carBooked.emit(carId);
  }

  onCarReturned(carId: number) {
    this.carReturned.emit(carId);
  }

  onMarkerClicked(car: Car) {
    this.panToAndEmitEvent(car);
  }

  onCarClicked(car: Car) {
    this.panToAndEmitEvent(car);
  }

  private panToAndEmitEvent(car: Car) {
    this.map.panTo({
      lat: car.latitude,
      lng: car.longitude,
    });
    this.carSelected.emit(car);
  }
}
