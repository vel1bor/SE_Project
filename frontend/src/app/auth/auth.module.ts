import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginDumbComponent} from './components/login-dumb/login-dumb.component';
import {SharedModule} from '../shared/shared.module';
import {AuthRoutingModule} from './auth-routing.module';
import {RegisterDumbComponent} from './components/register-dumb/register-dumb.component';
import {LoginComponent} from './containers/login/login.component';
import {RegisterComponent} from './containers/register/register.component';
import {StoreModule} from '@ngrx/store';
import {authReducer} from './reducers/auth.reducer';
import {EffectsModule} from '@ngrx/effects';
import {AuthEffects} from './effects/auth.effects';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    LoginDumbComponent,
    RegisterDumbComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AuthRoutingModule,
    StoreModule.forFeature('auth', authReducer),
    EffectsModule.forFeature([AuthEffects]),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AuthModule {
}
