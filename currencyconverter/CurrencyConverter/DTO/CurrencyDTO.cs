﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyConverter
{
    public class CurrencyDTO
    {
        public List<string> names { get; set; }

        public CurrencyDTO()
        {
            this.names = new List<string>();
        }
    }
}