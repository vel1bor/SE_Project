import {Injectable} from '@angular/core';
import {createSelector, select, Store} from '@ngrx/store';
import * as fromRoot from '../../../store';
import * as fromRental from '../../reducers/rental.reducer';
import {RentalState} from '../../reducers/rental.reducer';
import {BookCar, DeselectCar, GetAvailableCars, ReturnCar, SelectCar} from '../../actions/rental.actions';
import {Observable} from 'rxjs';
import {Car} from '../../../shared/models/car.model';

@Injectable({
  providedIn: 'root'
})
export class RentalFacade {

  private availableCars$ = this.store.pipe(
    select(createSelector(
      fromRoot.getRentalState,
      fromRental.getAvailableCars,
    ))
  );
  private bookedCars$ = this.store.pipe(
    select(createSelector(
      fromRoot.getRentalState,
      fromRental.getBookedCars,
    ))
  );
  private selectedCar$ = this.store.pipe(
    select(createSelector(
      fromRoot.getRentalState,
      fromRental.getSelectedCar,
    ))
  );

  getAvailableCars(): Observable<Car[]> {
    this.store.dispatch(GetAvailableCars());
    return this.availableCars$;
  }

  constructor(
    private store: Store<RentalState>,
  ) {
  }

  deselectCar(): void {
    this.store.dispatch(DeselectCar());
  }

  getBookedCars(): Observable<Car[]> {
    return this.bookedCars$;
  }

  getSelectedCar(): Observable<Car> {
    return this.selectedCar$;
  }

  selectCar(car: Car) {
    this.store.dispatch(SelectCar({car}));
  }

  returnCar(carId: number): void {
    this.store.dispatch(ReturnCar({carId}));
  }

  bookCar(carId: number): void {
    this.store.dispatch(BookCar({carId}));
  }
}
