package com.example.services;


import com.example.domain.Car;
import com.example.repos.CarRepo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class CarServiceTest {

    @Resource
    private CarRepo carRepo;


    @Test
    public void createCarTest() {
        // when
        Car car = new Car();
        car.setName("testVW");
        carRepo.save(car);

        // then
        List<Car> cars = carRepo.findAll();
        Assert.assertEquals("testVW", cars.get(0).getName());
    }

    @Test
    public void findAllTest() {
        // when
        List<Car> cars = carRepo.findAll();

        // then
        Assert.assertNotEquals(null, cars);
    }

    @Test
    public void getCarByIdTest() {
        // given
        Car car = new Car();
        car.setName("testVW");
        carRepo.save(car);

        // when
        Car result = carRepo.findById(new Long(3))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Car with this id does not exist"));

        // then
        Assert.assertEquals("testVW", result.getName());
    }

    @Test
    public void updateCarTest() {
        // given
        Car car = new Car();
        car.setId(new Long(2));
        car.setName("testFord");
        carRepo.save(car);

        // when
        if(!carRepo.existsById(car.getId())){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Car with this id does not exist");
        }
        carRepo.save(car);

        // then
        Car result = carRepo.findById(new Long(2))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Car with this id does not exist"));
        Assert.assertEquals("testFord", result.getName());
    }


}