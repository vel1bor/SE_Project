import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';
import {rentalReducer, RentalState} from '../rental/reducers/rental.reducer';
import {authReducer, AuthState} from '../auth/reducers/auth.reducer';
import {bookingReducer, BookingState} from '../booking/reducers/booking.reducer';

export interface AppState {
  auth: AuthState;
  rental: RentalState;
  bookings: BookingState;
}

export const appReducers: ActionReducerMap<AppState> = {
  auth: authReducer,
  rental: rentalReducer,
  bookings: bookingReducer,
};

export const getAuthState = createFeatureSelector<AuthState>('auth');
export const getRentalState = createFeatureSelector<RentalState>('rental');
export const getBookingState = createFeatureSelector<BookingState>('bookings');
