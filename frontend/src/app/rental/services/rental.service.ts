import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Car} from '../../shared/models/car.model';
import {Observable, throwError} from 'rxjs';
import {catchError, shareReplay} from 'rxjs/operators';
import {Booking} from '../../shared/models/booking.model';

@Injectable({
  providedIn: 'root'
})
export class RentalService {

  private commonHttpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    .set('Access-Control-Expose-Headers', 'Access-Control-Allow-Origin')
    .set('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE')
    .set('Access-Control-Allow-Origin', '*');

  constructor(
    private http: HttpClient,
  ) {
  }

  getAvailableCars(): Observable<Car[]> {
    const params = new HttpParams()
      .set('onlyAvailable', 'true');

    return this.http.get<Car[]>(
      `http://ec2-18-157-185-120.eu-central-1.compute.amazonaws.com:8080/api/cars`,
      {
        params,
        headers: this.commonHttpHeaders,
      }
    ).pipe(
      catchError(error => {
        return throwError(error);
      }),
      shareReplay(1),
    );
  }

  bookCar(carId: number): Observable<Booking> {
    const booking: Booking = {
      carId,
      id: undefined,
      startDate: undefined,
      endDate: undefined,
      car: undefined,
      price: undefined,
    };
    return this.http.post<Booking>(
      `http://ec2-18-157-185-120.eu-central-1.compute.amazonaws.com:8080/api/booking/opening`,
      booking,
      {
        headers: this.commonHttpHeaders,
      }
    ).pipe(
      catchError(error => {
        return throwError(error);
      }),
      shareReplay(1),
    );
  }

  returnCar(carId: number): Observable<Car> {
    return this.http.put<Car>(
      `http://ec2-18-157-185-120.eu-central-1.compute.amazonaws.com:8080/cars/return/${carId}`,
      undefined,
      {
        headers: this.commonHttpHeaders,
      }
    ).pipe(
      catchError(error => {
        return throwError(error);
      }),
      shareReplay(1),
    );
  }
}
