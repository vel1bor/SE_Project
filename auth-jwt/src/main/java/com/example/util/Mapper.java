package com.example.util;

import com.example.domain.User;
import com.example.resource.dtos.UserDto;
import com.example.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Mapper {

    private UserService userService;

    @Autowired
    public Mapper( final UserService userService) {
        this.userService = userService;
    }

    public User convertToDomain(UserDto userDto) {
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setCurrency(userDto.getCurrency());
        user.setEnabled(userDto.isEnabled());
        return user;
    }

    public UserDto convertToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setCurrency(user.getCurrency());
        userDto.setEnabled(user.isEnabled());
        return userDto;
    }

}
