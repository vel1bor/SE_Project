import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../models/user.model';
import {RegisterUser} from '../../models/register-user.model';

@Component({
  selector: 'app-register-dumb',
  templateUrl: './register-dumb.component.html',
  styleUrls: ['./register-dumb.component.css']
})
export class RegisterDumbComponent implements OnInit {

  registerForm: FormGroup;
  @Input() currencies: string[];
  @Output() register = new EventEmitter<User>();

  constructor(
    private fb: FormBuilder,
  ) {
    this.registerForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      passwordRepeat: ['', Validators.required],
      currency: ['USD', Validators.required],
    }, {validator: this.checkPasswords});
  }

  ngOnInit() {
  }

  onSubmit(userData: RegisterUser) {
    this.register.emit(userData);
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.get('password').value;
    const confirmPass = group.get('passwordRepeat').value;
    return pass === confirmPass ? null : {notSame: true};
  }
}
