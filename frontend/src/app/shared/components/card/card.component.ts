import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() title: string;
  @Input() subTitle: string;
  @Input() imgUrl: string;
  @Input() selected: boolean;
  @Input() titlePadding = true;
  @Input() subTitlePadding = true;
  @Input() cursorPointer: boolean;
  @Output() cardClick = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  onCardClicked() {
    this.cardClick.emit();
  }
}
