import {User} from './user.model';

export interface RegisterUser extends User {
  passwordRepeat: string;
}
