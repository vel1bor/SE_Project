import {Injectable} from '@angular/core';
import {createSelector, select, Store} from '@ngrx/store';
import * as fromRoot from '../../../store';
import * as fromBookings from '../../reducers/booking.reducer';
import {RentalState} from '../../../rental/reducers/rental.reducer';
import {GetBookings, StopBooking} from '../../actions/booking.actions';
import {Observable} from 'rxjs';
import {Booking} from '../../../shared/models/booking.model';

@Injectable({
  providedIn: 'root'
})
export class BookingFacade {

  private bookings$ = this.store.pipe(
    select(createSelector(
      fromRoot.getBookingState,
      fromBookings.getBookings,
    ))
  );

  constructor(
    private store: Store<RentalState>,
  ) {
  }

  getBookings(): Observable<Booking[]> {
    this.store.dispatch(GetBookings());
    return this.bookings$;
  }

  stopBooking(booking: Booking) {
    this.store.dispatch(StopBooking({booking}));
  }
}
