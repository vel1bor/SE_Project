package com.example.resource;

import com.example.domain.User;
import com.example.resource.dtos.UserDto;
import com.example.util.JwtUtil;
import com.example.services.MyUserDetailsService;
import com.example.services.UserService;
import com.example.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import static com.example.resource.BaseResource.USER;

@RestController
@RequestMapping(USER)
public class UserResource extends BaseResource {

    private final UserService userService;
    @Autowired
    protected Mapper mapper;

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @Autowired
    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @PostMapping(value = "/register")
    public ResponseEntity<UserDto> register(@RequestBody UserDto userDto) {
        User user = mapper.convertToDomain(userDto);
        userService.register(user);
        UserDto createdUser = mapper.convertToDto(user);
        return ResponseEntity.ok(createdUser);
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody UserDto userDto) throws Exception {

        User user = mapper.convertToDomain(userDto);
        user = userService.getUserByUsername(user.getUsername());
        if(!myUserDetailsService.validatePassword(user.getUsername(), userDto.getPassword())){
            System.out.println("Wrong credentials");
            return null;
        }

        return ResponseEntity.ok(mapper.convertToDto(user));
    }
}
