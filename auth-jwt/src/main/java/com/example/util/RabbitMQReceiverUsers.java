package com.example.util;

import com.example.domain.User;
import com.example.resource.dtos.RabbitUserDto;
import com.example.services.UserService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;

public class RabbitMQReceiverUsers {

    @Autowired
    private Mapper mapper;

    @Autowired
    private UserService userService;

    @RabbitListener(queues = "users")
    public RabbitUserDto receive2(RabbitUserDto userDto) throws InterruptedException {
        return receive(userDto);
    }


    public RabbitUserDto receive(RabbitUserDto userDto) {
//        StopWatch watch = new StopWatch();
//        watch.start();
        User user = userService.getUserByUsername(userDto.getUsername());
//        watch.stop();
        RabbitUserDto rabbitUserDto = new RabbitUserDto(user.getUsername(), user.getPassword());
        rabbitUserDto.setCurrency(user.getCurrency());
        return rabbitUserDto;
    }
}
